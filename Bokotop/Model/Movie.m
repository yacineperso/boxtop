//
//  Movie.m
//  Bokotop
//
//  Created by Yacine Salhi on 06/10/14.
//  Copyright (c) 2014 Yacine. All rights reserved.
//

#import "Movie.h"

@implementation Movie

- (id)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err {
    self = [super initWithDictionary:dict error:err];
    if (self) {
        _persoRate = [NSNumber numberWithFloat:0.];
        _similarMovies = nil;
    }
    return self;
}

// Get NSString of release date for theater and/or dvd
- (NSString *)formatStringReleaseDates {

    // Format theater date
    NSMutableString *res = [NSMutableString string];
    NSString *str = [_releaseDates objectForKey:@"theater"];
    NSDate *dt = [self dateFromString:str];
    if (dt) {
        str = [self stringFromDate:dt];
        [res appendString:[NSString stringWithFormat:@"Theater: %@", str]];
    }

    // Format dvd date
    str = [_releaseDates objectForKey:@"dvd"];
    if (str) {
        dt = [self dateFromString:str];
        if (dt) {
            str = [self stringFromDate:dt];
            [res appendString:[NSString stringWithFormat:@" / DVD: %@", str]];
        }
    }
    return res;
}

// Format forward - From NSString to NSDate
- (NSDate *)dateFromString:(NSString *)str {
    NSDateFormatter *dfForw = [[NSDateFormatter alloc] init];
    [dfForw setDateFormat:@"yyyy-MM-dd"];
    return [dfForw dateFromString:str];
}

// Format backward - From NSDate to NSString
- (NSString *)stringFromDate:(NSDate *)d {
    NSDateFormatter *dfBack = [[NSDateFormatter alloc] init];
    [dfBack setDateFormat:@"dd MMM YYYY"];
    return [dfBack stringFromDate:d];
}

#pragma mark -
#pragma mark - JsonKeyMapper

+ (JSONKeyMapper*)keyMapper
{
    return [JSONKeyMapper mapperFromUnderscoreCaseToCamelCase];
}

@end
