//
//  Movie.h
//  Bokotop
//
//  Created by Yacine Salhi on 06/10/14.
//  Copyright (c) 2014 Yacine. All rights reserved.
//

#import "JSONModel.h"

@protocol Movie

@end

@interface Movie : JSONModel

@property (nonatomic, assign)   NSInteger       id;
@property (nonatomic, strong)   NSString        *title;
@property (nonatomic, assign)   NSInteger       year;
@property (nonatomic, strong)   NSString        *mpaaRating;
@property (nonatomic, assign)   NSInteger       runtime;
@property (nonatomic, strong)   NSString        *criticsConsensus;
@property (nonatomic, strong)   NSString        *synopsis;
@property (nonatomic, strong)   NSDictionary    *releaseDates;
@property (nonatomic, strong)   NSDictionary    *ratings;
@property (nonatomic, strong)   NSDictionary    *posters;
@property (nonatomic, strong)   NSArray         *abridgedCast;
@property (nonatomic, strong)   NSDictionary    *links;

@property (nonatomic, strong)   NSNumber<Optional> *persoRate; // Personnal rate for movie
@property (nonatomic, strong)   NSArray<Optional, Movie>  *similarMovies;

- (NSString *)formatStringReleaseDates;

@end
