//
//  DataManager.h
//  Bokotop
//
//  Created by Yacine Salhi on 06/10/14.
//  Copyright (c) 2014 Yacine. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataManager : NSObject

+ (id)sharedManager;

- (NSArray *)movies;

- (void)updateMovieListWithArray:(NSArray *)array;
- (void)updateMovieListWithFileName:(NSString *)fileName;
- (void)updateMovie:(Movie *)movie withSimilarMovies:(NSArray *)arr;

- (void)storeInformation;


@end
