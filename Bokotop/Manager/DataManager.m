//
//  DataManager.m
//  Bokotop
//
//  Created by Yacine Salhi on 06/10/14.
//  Copyright (c) 2014 Yacine. All rights reserved.
//

#import <CocoaLumberjack/CocoaLumberjack.h>

#import "JSONModel.h"
#import "Movie.h"
#import "APIManager.h"
#import "DataManager.h"

#define FILE_NAME   @"boxmovies.plist"

#define MOVIE_STORE_KEY @"kMovieStoreKey"

@interface DataManager ()

@property (nonatomic, strong)   NSMutableArray *movies;

@end

static const int ddLogLevel = LOG_LEVEL_ALL;

@implementation DataManager

+ (id)sharedManager {
    static DataManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
        
    });
    return sharedMyManager;
}

- (id)init {
    self = [super init];
    if (self) {
        [self loadInformation];
    }
    return self;
}

//    [[APIManager sharedManager] fetchBoxOfficeMovies];

#pragma mark -
#pragma mark - Store - Load Methods

- (NSString *)pathForFileResource {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:FILE_NAME];
}

- (void)loadInformation {
    NSString *path = [self pathForFileResource];
    // We already have some data stored
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        NSData *d = [[NSData alloc] initWithContentsOfFile:path];
        NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:d];
        self.movies = [unarchiver decodeObjectForKey:MOVIE_STORE_KEY];
        [unarchiver finishDecoding];
    }
    else { // This is the first init
        self.movies = [NSMutableArray array];
        [self storeInformation];
    }
}

- (void)storeInformation {
    NSString *pathInfo = [self pathForFileResource];
    
    NSMutableData *data = [[NSMutableData alloc] init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    
    [archiver encodeObject:self.movies forKey:MOVIE_STORE_KEY];
    [archiver finishEncoding];
    BOOL res = [data writeToFile:pathInfo atomically:YES];
    if (!res)
        DDLogError(@"Could not store file %@", FILE_NAME);
}

#pragma mark -
#pragma mark - Movies Methods

- (NSArray *)movies {
    return _movies;
}

- (void)updateMovieListWithArray:(NSArray *)array {
    [_movies removeAllObjects];
    NSError *error = nil;
    Movie *tmp;
    for (NSDictionary *m in array) {
        error = nil;
        tmp = [[Movie alloc] initWithDictionary:m error:&error];
        if (!error)
            [_movies addObject:tmp];
        else
            DDLogError(@"Error adding movie %@", [error description]);
    }
    [self storeInformation];
}

// Load from file
- (void)updateMovieListWithFileName:(NSString *)fileName {
    [_movies removeAllObjects];
    NSError *error = nil;
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:nil];
    NSData *data = [[NSData alloc] initWithContentsOfFile:path];
    NSDictionary *obj = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    if (!error) {
        NSArray *movies = [obj objectForKey:@"movies"];
        Movie *tmp;
        for (NSDictionary *m in movies) {
            error = nil;
            tmp = [[Movie alloc] initWithDictionary:m error:&error];
            if (!error)
                [_movies addObject:tmp];
        }
    }
}

- (void)updateMovie:(Movie *)movie withSimilarMovies:(NSArray *)arr {
    NSMutableArray *tmpA = [NSMutableArray array];
    NSError *error;
    Movie *tmp;
    for (NSDictionary *m in arr) {
        error = nil;
        tmp = [[Movie alloc] initWithDictionary:m error:&error];
        if (!error)
            [tmpA addObject:tmp];
        else
            DDLogError(@"Error adding similar movie %@", [error description]);
    }
    [movie setSimilarMovies:(NSArray<Movie, Optional> *)[NSArray arrayWithArray:tmpA]];
    [self storeInformation];
}

@end