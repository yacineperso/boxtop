//
//  APIManager.m
//  Bokotop
//
//  Created by Yacine Salhi on 06/10/14.
//  Copyright (c) 2014 Yacine. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

#import "Movie.h"

#import "DataManager.h"

#import "APIManager.h"

#define REQUEST_HOST @"http://xebiamobiletest.herokuapp.com"
#define REQUEST_BOXOFFICE   @"/api/public/v1.0/lists/movies/box_office.json"

@interface APIManager () {
    BOOL    _connectionAvailable;
}

@end

@implementation APIManager

#pragma mark -
#pragma mark - Singleton

+ (id)sharedManager {
    static APIManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    self = [super init];
    if (self) {
        [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {

            if ((status == AFNetworkReachabilityStatusReachableViaWiFi) ||
                (status == AFNetworkReachabilityStatusReachableViaWWAN))
                _connectionAvailable = true;
            else
                _connectionAvailable = false;
        }];
    }
    return self;
}

- (BOOL)canPerformRequest {
    return _connectionAvailable;
}

#pragma mark -
#pragma mark - Requests

// Fetch latest box office
- (void)fetchBoxOfficeMovies {
    NSString *url = [NSString stringWithFormat:@"%@%@", REQUEST_HOST, REQUEST_BOXOFFICE];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        // Update Our Data Manager with the movie list
        [[DataManager sharedManager] updateMovieListWithArray:[responseObject objectForKey:@"movies"]];
        // Tell delegate that's cool
        if ((_apiDelegate) && ([_apiDelegate respondsToSelector:@selector(fetchBoxOfficeMoviesSuccess)])) {
            [_apiDelegate fetchBoxOfficeMoviesSuccess];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // Tell delegate that's not cool because request failure
        if ((_apiDelegate) && ([_apiDelegate respondsToSelector:@selector(fetchBoxOfficeMoviesFailed)])) {
            [_apiDelegate fetchBoxOfficeMoviesFailed];
        }
    }];
}

// Fetch latest box office
- (void)fetchSimilarMoviesforMovie:(Movie *)movie {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[[movie links] objectForKey:@"similar"] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {

        // Update Our Data Manager with the movie list
        [[DataManager sharedManager] updateMovie:movie withSimilarMovies:[responseObject objectForKey:@"movies"]];
        // Tell delegate that's cool
        if ((_apiDelegate) && ([_apiDelegate respondsToSelector:@selector(fetchSimilarMoviesForMovieSucceed)])) {
            [_apiDelegate fetchSimilarMoviesForMovieSucceed];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // Tell delegate that's not cool because request failure
        if ((_apiDelegate) && ([_apiDelegate respondsToSelector:@selector(fetchSimilarMoviesForMovieFailed)])) {
            [_apiDelegate fetchSimilarMoviesForMovieFailed];
        }
    }];
}

@end
