//
//  APIManager.h
//  Bokotop
//
//  Created by Yacine Salhi on 06/10/14.
//  Copyright (c) 2014 Yacine. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Movie;

@protocol APIManagerProtocol <NSObject>

@optional
- (void)fetchBoxOfficeMoviesSuccess;
- (void)fetchBoxOfficeMoviesFailed;

- (void)fetchSimilarMoviesForMovieSucceed;
- (void)fetchSimilarMoviesForMovieFailed;

@end

@interface APIManager : NSObject

@property (nonatomic, assign) id<APIManagerProtocol>    apiDelegate;

+ (id)sharedManager;

- (void)fetchBoxOfficeMovies;
- (void)fetchSimilarMoviesforMovie:(Movie *)movie;

@end
