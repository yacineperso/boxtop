//
//  AppDelegate.h
//  Bokotop
//
//  Created by Yacine Salhi on 06/10/14.
//  Copyright (c) 2014 Yacine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

