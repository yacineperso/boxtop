//
//  CastTableViewCell.h
//  Bokotop
//
//  Created by Yacine Salhi on 10/10/14.
//  Copyright (c) 2014 Yacine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CastTableViewCell : UITableViewCell

- (void)configureCellWithCharacter:(NSDictionary *)c;

@end
