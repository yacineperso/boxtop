//
//  MovieHeaderView.m
//  Bokotop
//
//  Created by Yacine Salhi on 07/10/14.
//  Copyright (c) 2014 Yacine. All rights reserved.
//

#import <JMImageCache/JMImageCache.h>
#import <RateView/RateView.h>
#import <CocoaLumberjack/CocoaLumberjack.h>

#import "Movie.h"
#import "DataManager.h"

#import "MovieHeaderView.h"

@interface MovieHeaderView () <RateViewDelegate> {
    Movie *_actualMovie;
}

@property (nonatomic, strong) IBOutlet  UIImageView *imgProfile;
@property (nonatomic, strong) IBOutlet  UILabel     *labelTitle;
@property (nonatomic, strong) IBOutlet  UILabel     *releaseDate;
@property (nonatomic, strong) IBOutlet  RateView    *criticsView;
@property (nonatomic, strong) IBOutlet  RateView    *audienceView;
@property (nonatomic, strong) IBOutlet  RateView    *persoView;

@end

static const int ddLogLevel = LOG_LEVEL_OFF;

@implementation MovieHeaderView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)configureHeaderViewWithMovie:(Movie *)m {
    _actualMovie = m;
    UIColor *starFillColor = [UIColor colorWithRed:(178/255.) green:(208/255.) blue:(165/255.) alpha:1.0];

    // Set label data
    [_labelTitle setText:m.title];
    [_releaseDate setText:[m formatStringReleaseDates]];

    // Set rating Views
    _criticsView.starFillColor = starFillColor;
    [_criticsView setRating:([[[m ratings] objectForKey:@"critics_score"] integerValue] * 5 / 100.)];
    DDLogInfo(@"Critics %li", (long)[[[m ratings] objectForKey:@"critics_score"] integerValue]);
    _audienceView.starFillColor = starFillColor;
    [_audienceView setRating:([[[m ratings] objectForKey:@"audience_score"] integerValue] * 5 / 100.)];
    DDLogInfo(@"Audience %li", (long)[[[m ratings] objectForKey:@"audience_score"] integerValue]);
    _persoView.starFillColor = starFillColor;
    [_persoView setRating:[[m persoRate] floatValue]];
    _persoView.canRate = YES;
    _persoView.delegate = self;
    
    // Set Image Profile Movie
    NSURL *url = [NSURL URLWithString:[[m posters] objectForKey:@"profile"]];
    if (url) {
        [[JMImageCache sharedCache] imageForURL:url completionBlock:^(UIImage *downloadedImage) {
            _imgProfile.image = downloadedImage;
        }];
    }
}

#pragma mark -
#pragma mark - Rate View Delegate

- (void)rateView:(RateView *)rateView didUpdateRating:(float)rating {
    if (rateView == _persoView) {
        [_actualMovie setPersoRate:[NSNumber numberWithFloat:rating]];
        [[DataManager sharedManager] storeInformation];
    }
}

@end
