//
//  CastTableViewCell.m
//  Bokotop
//
//  Created by Yacine Salhi on 10/10/14.
//  Copyright (c) 2014 Yacine. All rights reserved.
//

#import "CastTableViewCell.h"

@interface CastTableViewCell ()

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UILabel *detailTitleLabel;

@end

@implementation CastTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureCellWithCharacter:(NSDictionary *)c {
    [_titleLabel setText:[c objectForKey:@"name"]];

    [_detailTitleLabel setText:[self formatCharactersPlayed:[c objectForKey:@"characters"]]];
}

// Format string of all character played by a star
- (NSString *)formatCharactersPlayed:(NSArray *)arr {
    NSMutableString *str = [NSMutableString string];
    NSString *last = [arr lastObject];
    for (NSString *name in arr) {
        [str appendString:name];
        if (![name isEqualToString:last])
            [str appendString:@", "];
    }
    return str;
}

@end
