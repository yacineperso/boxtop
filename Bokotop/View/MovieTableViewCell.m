//
//  MovieTableViewCell.m
//  Bokotop
//
//  Created by Yacine Salhi on 06/10/14.
//  Copyright (c) 2014 Yacine. All rights reserved.
//


#import <JMImageCache/JMImageCache.h>

#import "Movie.h"

#import "MovieTableViewCell.h"

@interface MovieTableViewCell ()

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UIImageView *thumbnail;

@end

@implementation MovieTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)configureCellWithMovie:(Movie *)m {
    [_titleLabel setText:m.title];

    NSURL *url = [NSURL URLWithString:[[m posters] objectForKey:@"thumbnail"]];
    if (url) {
        [[JMImageCache sharedCache] imageForURL:url completionBlock:^(UIImage *downloadedImage) {
            _thumbnail.image = downloadedImage;
        }];
    }
}

@end
