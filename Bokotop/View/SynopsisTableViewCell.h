//
//  SynopsisTableViewCell.h
//  Bokotop
//
//  Created by Yacine Salhi on 11/10/14.
//  Copyright (c) 2014 Yacine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SynopsisTableViewCell : UITableViewCell

- (void)configureSynopsis:(NSString *)syn;
- (CGFloat)estimatedHeight;

@end
