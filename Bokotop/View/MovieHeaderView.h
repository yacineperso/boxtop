//
//  MovieHeaderView.h
//  Bokotop
//
//  Created by Yacine Salhi on 07/10/14.
//  Copyright (c) 2014 Yacine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MovieHeaderView : UIView

- (void)configureHeaderViewWithMovie:(Movie *)m;

@end
