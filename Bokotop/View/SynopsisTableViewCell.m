//
//  SynopsisTableViewCell.m
//  Bokotop
//
//  Created by Yacine Salhi on 11/10/14.
//  Copyright (c) 2014 Yacine. All rights reserved.
//

#import "SynopsisTableViewCell.h"

@interface SynopsisTableViewCell ()

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;

@end

@implementation SynopsisTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)configureSynopsis:(NSString *)syn {
    [self.titleLabel setText:syn];
    [self.titleLabel sizeToFit];
}

- (CGFloat)estimatedHeight {
    CGSize s = CGSizeMake(_titleLabel.frame.size.width, 9999);
    CGRect rect = [_titleLabel.text boundingRectWithSize:s
                                                 options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                              attributes:@{NSFontAttributeName:_titleLabel.font}
                                                 context:nil];

    
    // Auto Layout we multiple by 2 if iphone
    // or we could devide by 2 the width if iphone
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        return rect.size.height * 2;

    return rect.size.height;
}

@end
