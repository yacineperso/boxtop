//
//  BaseMovieListViewController.h
//  Bokotop
//
//  Created by Yacine Salhi on 13/10/14.
//  Copyright (c) 2014 Yacine. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString * const kMovieCellIdentifier = @"MovieCell";

@interface BaseMovieListViewController : UITableViewController {
    UIColor *_color1;
    UIColor *_color2;
}

@end
