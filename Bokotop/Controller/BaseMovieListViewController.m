//
//  BaseMovieListViewController.m
//  Bokotop
//
//  Created by Yacine Salhi on 13/10/14.
//  Copyright (c) 2014 Yacine. All rights reserved.
//

#import "MovieTableViewCell.h"

#import "BaseMovieListViewController.h"

@interface BaseMovieListViewController ()

@end

@implementation BaseMovieListViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.tableView registerNib:[UINib nibWithNibName:@"MovieCell" bundle:nil] forCellReuseIdentifier:kMovieCellIdentifier];
    
    // Init Default Color Cell
    _color1 = [UIColor colorWithRed:(242/255.) green:(242/255.) blue:(242/255.) alpha:1.0];
    _color2 = [UIColor colorWithRed:(178/255.) green:(208/255.) blue:(165/255.) alpha:1.0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100.;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MovieTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kMovieCellIdentifier forIndexPath:indexPath];
    
    if (indexPath.row % 2)
        [cell setBackgroundColor:_color1];
    else
        [cell setBackgroundColor:_color2];
    return cell;
}

@end
