//
//  MovieDetailViewController.h
//  Bokotop
//
//  Created by Yacine Salhi on 06/10/14.
//  Copyright (c) 2014 Yacine. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Movie;

@interface MovieDetailViewController : UIViewController

@property (nonatomic, strong) Movie *movie;

@end
