//
//  BoxOfficeViewController.m
//  Bokotop
//
//  Created by Yacine Salhi on 06/10/14.
//  Copyright (c) 2014 Yacine. All rights reserved.
//

#import <CocoaLumberjack/CocoaLumberjack.h>

#import "Movie.h"
#import "APIManager.h"
#import "DataManager.h"

#import "MovieTableViewCell.h"
#import "MovieDetailViewController.h"
#import "FilteredMovieListViewController.h"

#import "BoxOfficeViewController.h"

@interface BoxOfficeViewController () <UISearchResultsUpdating, UISearchBarDelegate, APIManagerProtocol> {
    NSArray *_objects;
    BOOL _updatingList;
    UIView  *_backgroudSearch;
}

@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) FilteredMovieListViewController *filteredController;

@end

static const int ddLogLevel = LOG_LEVEL_ALL;

@implementation BoxOfficeViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // We fetch the latest box office information
    [[APIManager sharedManager] setApiDelegate:self];
    [[APIManager sharedManager] fetchBoxOfficeMovies];
    //    [[DataManager sharedManager] updateMovieListWithFileName:@"box_office.json"];
    _objects = [[DataManager sharedManager] movies];

    // Init Filtered Controller
    _filteredController = [[FilteredMovieListViewController alloc] init];
    _filteredController.tableView.delegate = self;
    [_filteredController setFilteredArray:[NSMutableArray arrayWithArray:_objects]];

    // Init Search Controller
    _searchController = [[UISearchController alloc] initWithSearchResultsController:_filteredController];
    _searchController.dimsBackgroundDuringPresentation = NO;
    _searchController.hidesNavigationBarDuringPresentation = NO;
    _searchController.searchResultsUpdater = self;
    _searchController.searchBar.delegate = self;
    _searchController.searchBar.frame = CGRectMake(0, 0, self.navigationController.navigationBar.frame.size.width / 3, self.navigationController.navigationBar.frame.size.height);
    self.definesPresentationContext = YES;

    // Set up search bar view
    _backgroudSearch = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.navigationController.navigationBar.frame.size.width / 3, self.navigationController.navigationBar.frame.size.height)];
    [_backgroudSearch addSubview:_searchController.searchBar];
     UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:_backgroudSearch];
     UIBarButtonItem *refreshItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshListMovies)];
    self.navigationItem.rightBarButtonItems = @[refreshItem, searchItem];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    // restore the searchbar frame if active state
    if (self.searchController.active) {
        _backgroudSearch.frame = CGRectMake(0, 0, self.navigationController.navigationBar.frame.size.width / 1.2, self.navigationController.navigationBar.frame.size.height);
        _searchController.searchBar.frame = _backgroudSearch.frame;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    _backgroudSearch.frame = CGRectMake(0, 0, self.navigationController.navigationBar.frame.size.width / 1.2, self.navigationController.navigationBar.frame.size.height);
    searchBar.frame = _backgroudSearch.frame;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    _backgroudSearch.frame = CGRectMake(0, 0, self.navigationController.navigationBar.frame.size.width / 3, self.navigationController.navigationBar.frame.size.height);
    searchBar.frame = _backgroudSearch.frame;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    _backgroudSearch.frame = CGRectMake(0, 0, self.navigationController.navigationBar.frame.size.width / 3, self.navigationController.navigationBar.frame.size.height);
    searchBar.frame = _backgroudSearch.frame;
}

#pragma mark - UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {

    NSString *searchString = [_searchController.searchBar text];
    [self updateFilteredContentForMovieName:searchString];

    [((UITableViewController *)searchController.searchResultsController).tableView reloadData];
}

- (void)updateFilteredContentForMovieName:(NSString *)movieName {
    NSMutableArray *pFilt = _filteredController.filteredArray;
    // Update the filtered array based on the search text
    if ((movieName == nil) || [movieName length] == 0) {
        pFilt = [NSMutableArray arrayWithArray:_objects];
    }
    else {
        [pFilt removeAllObjects];
        for (Movie *movie in _objects) {
            if ([movie.title rangeOfString:movieName options:NSCaseInsensitiveSearch].location != NSNotFound)
                [pFilt addObject:movie];
        }
    }
}

#pragma mark -
#pragma mark - UIBarButtonAction

- (void)refreshListMovies {
    [[APIManager sharedManager] fetchBoxOfficeMovies];
}

#pragma mark -
#pragma mark - APIManagerDelegate

- (void)fetchBoxOfficeMoviesSuccess {
    DDLogInfo(@"Fetch Box Office Success");
    _objects = [[DataManager sharedManager] movies];
    [self.tableView reloadData];
}

- (void)fetchBoxOfficeMoviesFailed {
    DDLogWarn(@"Fetch Box Office Warning");
}

#pragma mark -
#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_objects count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MovieTableViewCell *cell = (MovieTableViewCell *)[super tableView:tableView cellForRowAtIndexPath:indexPath];

    [cell configureCellWithMovie:[_objects objectAtIndex:indexPath.row]];
    return cell;
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MovieDetailViewController *detailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MovieDetailViewController"];
    if (self.searchController.isActive == YES) {
        detailViewController.movie = [_filteredController.filteredArray objectAtIndex:indexPath.row];
    }
    else {
        detailViewController.movie = [_objects objectAtIndex:indexPath.row];
    }

    [self.navigationController pushViewController:detailViewController animated:YES];
}

@end
