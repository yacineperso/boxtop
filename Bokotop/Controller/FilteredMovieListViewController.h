//
//  FilteredMovieListViewController.h
//  Bokotop
//
//  Created by Yacine Salhi on 13/10/14.
//  Copyright (c) 2014 Yacine. All rights reserved.
//

#import "BaseMovieListViewController.h"

@interface FilteredMovieListViewController : BaseMovieListViewController

@property (nonatomic, strong)   NSMutableArray *filteredArray;

@end
