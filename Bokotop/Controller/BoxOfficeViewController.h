//
//  BoxOfficeViewController.h
//  Bokotop
//
//  Created by Yacine Salhi on 06/10/14.
//  Copyright (c) 2014 Yacine. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseMovieListViewController.h"

@interface BoxOfficeViewController : BaseMovieListViewController

@end

