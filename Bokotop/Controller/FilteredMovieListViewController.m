//
//  FilteredMovieListViewController.m
//  Bokotop
//
//  Created by Yacine Salhi on 13/10/14.
//  Copyright (c) 2014 Yacine. All rights reserved.
//

#import "MovieTableViewCell.h"

#import "FilteredMovieListViewController.h"

@interface FilteredMovieListViewController ()

@end

@implementation FilteredMovieListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _filteredArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MovieTableViewCell *cell = (MovieTableViewCell *)[super tableView:tableView cellForRowAtIndexPath:indexPath];

    [cell configureCellWithMovie:[_filteredArray objectAtIndex:indexPath.row]];
    return cell;
}


@end
