//
//  MovieDetailViewController.m
//  Bokotop
//
//  Created by Yacine Salhi on 06/10/14.
//  Copyright (c) 2014 Yacine. All rights reserved.
//

#import <CocoaLumberjack/CocoaLumberjack.h>

#import "Movie.h"
#import "MovieHeaderView.h"
#import "SynopsisTableViewCell.h"
#import "CastTableViewCell.h"
#import "MovieTableViewCell.h"
#import "APIManager.h"

#import "MovieDetailViewController.h"

typedef enum : NSUInteger {
    SectionMovieDetailSynopsis = 0,
    SectionMovieDetailCasting,
    SectionMovieDetailSimilar, // Similar Movies
    SectionMovieDetailTotal
} SectionMovieDetail;

@interface MovieDetailViewController () <UITableViewDataSource, UITableViewDelegate, APIManagerProtocol> {
    NSArray *_casting;
    NSArray *_similar;
    UIColor *_color1;
    UIColor *_color2;
}

@property (nonatomic, strong)   IBOutlet UITableView *tableView;
@property (nonatomic, strong)   IBOutlet MovieHeaderView *headerView;

@end

@implementation MovieDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _casting = [_movie abridgedCast];
    _similar = [_movie similarMovies];
    
    // Init Default Color Cell
    _color1 = [UIColor colorWithRed:(242/255.) green:(242/255.) blue:(242/255.) alpha:1.0];
    _color2 = [UIColor colorWithRed:(178/255.) green:(208/255.) blue:(165/255.) alpha:1.0];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"MovieCell" bundle:nil] forCellReuseIdentifier:@"MovieCell"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    // Fetch similar movies
    if (!_movie.similarMovies) {
        [[APIManager sharedManager] setApiDelegate:self];
        [[APIManager sharedManager] fetchSimilarMoviesforMovie:_movie];
    }
    
    // Configure header view with movie info
    [_headerView configureHeaderViewWithMovie:_movie];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[APIManager sharedManager] setApiDelegate:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - APIManager Similar Movies Fetch

- (void)fetchSimilarMoviesForMovieSucceed {
    _similar = _movie.similarMovies;
    [_tableView reloadSections:[NSIndexSet indexSetWithIndex:SectionMovieDetailSimilar] withRowAnimation:UITableViewRowAnimationLeft];
}

- (void)fetchSimilarMoviesForMovieFailed {
    
}

#pragma mark -
#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return SectionMovieDetailTotal;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger nb;
    switch (section) {
        case SectionMovieDetailSynopsis:
            nb = ((!_movie.synopsis.length) ? (0) : (1));
            break;

        case SectionMovieDetailSimilar:
            nb = [_similar count];
            break;

        case SectionMovieDetailCasting:
            nb = [_casting count];
            break;
            
        default:
            nb = 0;
            break;
    }
    return nb;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *str;
    switch (section) {
        case SectionMovieDetailSynopsis:
            str = @"Synopsis";
            break;
            
        case SectionMovieDetailSimilar:
            str = @"Similar movies";
            break;
            
        case SectionMovieDetailCasting:
            str = @"Casting";
            break;

        default:
            str = @"";
            break;
    }
    return str;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat h;
    switch (indexPath.section) {
        case SectionMovieDetailSynopsis:
            h = [[self synopsisCell] estimatedHeight];
            break;
        
        case SectionMovieDetailCasting:
            h = 98.;
            break;
            
        case SectionMovieDetailSimilar:
            h = 100.;
            break;
            
        default:
            break;
    }
    return h;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    switch (indexPath.section) {
        case SectionMovieDetailSynopsis:
            cell = [self synopsisCell];
            break;

        case SectionMovieDetailCasting:
            cell = [self castCellForIndexPath:indexPath];
            break;
            
        case SectionMovieDetailSimilar:
            cell = [self movieCellForIndexPath:indexPath];
            break;
            
        default:
            break;
    }

    if (indexPath.row % 2)
        [cell setBackgroundColor:_color1];
    else
        [cell setBackgroundColor:_color2];
    return cell;
}

- (SynopsisTableViewCell *)synopsisCell {
    static NSString *cIdentifier = @"SynCell";
    SynopsisTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:cIdentifier];
    [cell configureSynopsis:_movie.synopsis];
    return cell;
}

- (UITableViewCell *)castCellForIndexPath:(NSIndexPath *)indexPath {
    static NSString *cIdentifier = @"CastCell";
    CastTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:cIdentifier forIndexPath:indexPath];
    [cell configureCellWithCharacter:[_casting objectAtIndex:indexPath.row]];
    return cell;
}

- (UITableViewCell *)movieCellForIndexPath:(NSIndexPath *)indexPath {
    static NSString *cIdentifier = @"MovieCell";
    MovieTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:cIdentifier forIndexPath:indexPath];
    [cell configureCellWithMovie:[_similar objectAtIndex:indexPath.row]];
    return cell;
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == SectionMovieDetailSimilar) {
        MovieDetailViewController *detailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MovieDetailViewController"];
        detailViewController.movie = [_similar objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
}

@end
